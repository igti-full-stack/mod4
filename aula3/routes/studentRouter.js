import express from 'express'

import { studentModel } from '../models/studentModel.js'

const app = express()

app.post('/student', async (req, res) => {
  try {
    const student = new studentModel(req.body)
    await student.save()

    res.send(student)
  } catch (error) {
    console.log('Erro ao criar um novo aluno')
    res.status(500).send(error)
  }
})

app.get('/student', async (req, res) => {
  try {
    const students = await studentModel.find({})
    res.send(students)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao trazer a lista de alunos')
  }
})

app.patch('/student/:id', async (req, res) => {
  try {
    const id = req.params.id

    const data = await studentModel.findByIdAndUpdate({_id: id}, req.body, {
      new: true,
    })

    res.send(data)

  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao atualizar aluno')
  }
})

app.delete('/student/:id', async (req, res) => {
  try {
    const student = await studentModel.findByIdAndDelete({_id: req.params.id})
    console.log('Aluno excluído com sucesso!')

    if(!student) {
      res.status(400).send('Documento não encontrado na coleção')
    } else {
      res.status(200).send('OK')
    }
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao excluir aluno')
  }
})

export { app as studentRouter }
