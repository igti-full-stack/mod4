import express from 'express'
import mongoose from 'mongoose'

import { studentRouter } from './routes/studentRouter.js'

(async () => {
  try {
    await mongoose.connect('mongodb://172.17.0.2:27017/grades2', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    console.log('Conectado com sucesso!')
  } catch (error) {
    console.log('Erro ao conectar ao banco mongoDB')
    console.log(error)
  }
})()

const app = express()

app.use(express.json())
app.use(studentRouter)

app.listen(3000, () => {
  console.log('API iniciada!')
})
