import mongoose from 'mongoose'

(async () => {
  try {
    await mongoose.connect('mongodb://172.17.0.2:27017/grades2', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  } catch (error) {
    console.log('Erro ao conectar ao banco mongoDB')
    console.log(error)
  }
})()

const studentSchema = mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  subject: {
    type: String,
    require: true,
  },
  type: {
    type: String,
    require: true,
  },
  value: {
    type: Number,
    require: true,    
    min: 0,
  },
  lastModified: {
    type: Date,
    default: Date.now,
  }

})

const studentModel = mongoose.model('student', studentSchema, 'student')

export { studentModel }
