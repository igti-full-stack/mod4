import mongoose from 'mongoose'

(async () => {
  try {
    await mongoose.connect('mongodb://172.17.0.2:27017/grades2', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })    
  } catch (error) {
    console.log('Erro ao conectar ao banco mongoDB')
    console.log(error)
  }  
})()


const studentSchema = mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  subject: {
    type: String,
    require: true,
  },
  type: {
    type: String,
    require: true,
  },
  value: {
    type: Number,
    require: true,
  },
  lastModified: {
    type: Date,
    default: Date.now,
  }

})

mongoose.model('student', studentSchema, 'student')

const student = mongoose.model('student')

new student({
  name: "Paulo Assis",
  subject: "Matematica",
  type: "Trabalho Pratico",
  value: 22
}).save().then(() => {
  console.log('documento inserido')
}).catch((err) => {
  console.log('Erro ao inserir documento')
  console.log(err)
})


