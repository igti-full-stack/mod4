const { MongoClient } = require("mongodb")
// Connection URI
const uri =
  "mongodb://172.17.0.2:27017/?poolSize=20&w=majority"

const client = new MongoClient(uri);
// const client = new MongoClient(uri, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
// })

client.connect(async (err) => {
  const collection = client.db('grades2').collection('student')
  const documents = await collection.find({ subject: "Historia" }).toArray()

  // console.log(documents)  // lista os documentos com assunto História

  const databaseList = await client.db().admin().listDatabases()

  console.log('Databases: ')

  databaseList.databases.forEach(db => {
    console.log(` - ${db.name}`)
  })

  client.close()
})

