import express from 'express'
// require('dotenv').config()

import { accountRouter } from './routes/accountRouter.js'
import { db } from './models/index.js'

(async () => {
  try {
    console.log('CONECTANCO AO MONGODB...')
    await db.mongoose.connect(db.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    console.log('CONECTADO COM SUCESSO!')
  } catch (error) {
    console.log('Erro ao conectar no MongoDB!')
    console.log(error)
  }
})()

const app = express()

app.use(express.json())
app.use(accountRouter)


app.listen(process.env.PORT || 3001, () => {
  console.log('API Bank started...')
})
