import { db } from '../models/index.js'

const Account = db.account

const deposit = async (req, res) => {
  const { _id, agencia, conta, deposit } = req.body
  try {
    let newDeposit = await validateAccount({ _id, agencia, conta, deposit })
    newDeposit.balance += deposit
    newDeposit = new Account(newDeposit)

    await newDeposit.save()
    res.send(newDeposit)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao depositar ' + error.message)
  }
}

const withdraw = async (req, res) => {
  const { _id, agencia, conta, withdraw } = req.body
  const TAXA_SAQUE = 1
  try {
    let newDrayMoney = await validateAccount({ _id, agencia, conta, withdraw })
    newDrayMoney.balance -= withdraw + TAXA_SAQUE

    if (newDrayMoney.balance < 0) {
      throw new Error('Saldo insuficiente')
    }
    newDrayMoney = new Account(newDrayMoney)

    await newDrayMoney.save()
    res.send(newDrayMoney)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao sacar: ' + error.message)
  }
}

const checkBalance = async (req, res) => {
  const agencia = req.params.agencia
  const conta = req.params.conta
  try {
    const checkBalance = await validateAccount({ agencia, conta })
    res.send(checkBalance)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao consultar o saldo: ' + error.message)
  }
}

const remove = async (req, res) => {
  const { _id, agencia, conta } = req.body

  try {
    let deleteAccount = await validateAccount({ _id, agencia, conta })

    await Account.findByIdAndRemove({ _id: deleteAccount._id })
    deleteAccount = await Account.find({
      agencia: deleteAccount.agencia
    }).countDocuments()

    res.send({ totalAccounts: deleteAccount })
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao excluir a conta ' + error.message)
  }
}

const transfer = async (req, res) => {
  const account = req.body
  const transferMoney = account.valor
  const TAXA = 8

  try {
    let sourceAccount = await validateAccount({ conta: account.contaOrigem.conta })
    let targetAccount = await validateAccount({ conta: account.contaDestino.conta })

    if (sourceAccount.agencia !== targetAccount.agencia) {
      sourceAccount.balance -= TAXA
    }
    sourceAccount.balance -= transferMoney

    if (sourceAccount.balance < 0) {
      throw new Error('Saldo insuficiente para efeturar a transferência')
    }

    targetAccount.balance += transferMoney
    sourceAccount = new Account(sourceAccount)
    await sourceAccount.save()

    targetAccount = new Account(targetAccount)
    await targetAccount.save()

    res.send([sourceAccount, targetAccount])
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao realizar transferência: ' + error.message)
  }
}

const topByBalanceLowest = async (req, res) => {
  const limit = req.params.limit

  try {
    const account = await Account.find(
      {},
      { _id: 0, agencia: 1, conta: 1, balance: 1 }
    )
      .limit(parseInt(limit))
      .sort({ balance: 1 })

    if (account.length === 0) {
      throw new Error('Nenhum cliente encontrado')
    }

    res.send(account)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao obter lista de clientes: ' + error.message)
  }
}

const topByBalanceHighest = async (req, res) => {

  const limit = req.params.limit

  try {
    const account = await Account.find(
      {},
      { _id: 0, agencia: 1, conta: 1, nome: 1, balance: 1 }
    )
      .limit(parseInt(limit))
      .sort({ balance: -1, nome: 1 })

    if (account.length === 0) {
      throw new Error('Nenhum cliente encontrado')
    }

    res.send(account)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao obter lista de clientes: ' + error.message)
  }
}

const avgBalance = async (req, res) => {

  const agencia = req.params.agencia

  try {
    const averageBalance = await Account.aggregate([
      {
        $match: {
          agencia: parseInt(agencia),
        },
      },
      {
        $group: {
          _id: '$agencia',
          media: {
            $avg: '$balance',
          },
        },
      },
      {
        $project: {
          _id: 0,
          media: 1,
        },
      },
    ])

    if (averageBalance.length === 0) {
      throw new Error('Agencia não encontrada')
    }

    res.send(averageBalance)
  } catch (error) {
    console.log(error)
    res.status(500).send('Erro ao obter saldo médio da Agência: ', error.message)
  }
}

const transferToPrivate = async (req, res) => {

  try {
    let transferToPrivates = await Account.aggregate([
      {
        $group: {
          _id: '$agencia',
          balance: { $max: '$balance' },
        },
      },
    ])

    for (const transferToPrivate of transferToPrivates) {
      const { _id, balance } = transferToPrivate
      const PRIVATE_AGENCY = 99

      let newAccount = await Account.findOne({
        agencia: _id,
        balance,
      })
      newAccount.agencia = PRIVATE_AGENCY

      newAccount.save()
    }

    transferToPrivates = await Account.find({
      agencia: 99,
    })

    res.send(transferToPrivates)

  } catch (error) {
    console.log(error)
    throw new Error('Erro ao transferir clientes para a conta privada: ' + error.message)
  }
}

const validateAccount = async (account) => {
  const { agencia, conta } = account
  account = {
    agencia,
    conta,
  }
  try {
    if (typeof account.agencia !== 'undefined') {
      account = await Account.findOne(account)
    } else {
      account = await Account.findOne({ conta: account.conta })
    }

    if (!account) {
      throw new Error(`${agencia}/${conta} agencia/conta inválida`)
    }

    return account
  } catch (error) {
    console.log(error)
    throw new Error(error.message)
  }
}

export default {
  deposit,
  withdraw,
  checkBalance,
  remove,
  transfer,
  avgBalance,
  topByBalanceLowest,
  topByBalanceHighest,
  transferToPrivate,
}


